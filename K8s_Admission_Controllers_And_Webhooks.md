# Overview

We'll take a look at the two types of Admission Controllers as well as the
alternative, Admission Webhooks. These fulfill somewhat similar purposes but
have two major differences:

1. **Admission Controllers** are _compiled in_ to the `kube-apiserver` and can only
   enabled/dsiabled on startup of the cluster
1. **Admission Webhooks** are dynamic and can be added/removed at any time
   * They also require certain `AdmissionController`s to be enabled to work

## Admission Controllers

These controllers are essentially the "gatekeepers" of K8s. They intercept
requests and can change them or deny them entirely. They're a way to allow
K8s to stay abstracted from the exact rules needed for a cluster's policies
and required best practices. 

### Why ACs?

From K8s perspective, ACs are there to enable evolution of your policies
and capabilites for Objects in K8s without requiring K8s to be designed
for those specifically. 

From the perspective of a K8s admin, they're needed to go beyond the defaults.
K8s' defaults are largely chosen for ease of use and simplicity. This can result
in breaking some security best practices. For example, containers are by default
set to run as root in the cluster. You could use admission controllers 
_(or webhooks)_ to prevent this in all user containers.
### Enabling

You can enable or disable these by sending one of the following flags to the
`kube-apiserver` **when starting/creating your cluster**:

* **`--enable-admission-plugins`** Enables a comma-delimited list of controllers
* **`--disable-admission-plugins`** Disables a comma-delimited list of controllers

The two you need to have enabled are `MutatingAdmissionWebhook` and `ValidatingAdmissionWebhook`. These will enable us to create and use our own webhooks.

### Viewing Enabled Controllers and Versions

You can view if the admission controllers were enabled with `kubectl`:

```bash
# Check that "admission registration" is turned on, and for which
# version
kubectl api-versions | grep admissionregistration

# You'll see something like:
# admissionregistration.k8s.io/v1
# admissionregistration.k8s.io/v1beta1
```

#### Finding Exact Controllers Enabled

There's no simple way to find these out. You can go into the master
node on which the api server is running and look into the logs to 
see what flags were passed, but an easier way is to use `kubectl`
_if_ you know the name of the controller and the version you're
looking for.

For the below two examples, you can install `jq` to view the nice
JSON output.

```bash
# We're looking for the validating webhook controller with "v1"
kubectl get --raw /apis/admissionregistration.k8s.io/v1/validatingwebhookconfigurations | jq

# If it's enabled, it'll print something like:
# {
#   "kind": "ValidatingWebhookConfigurationList",
#   "apiVersion": "admissionregistration.k8s.io/v1",
#   "metadata": {
#     "resourceVersion": "2633"
#   },
#   "items": []
# }

# Now you can get extra info on it by walking down the path (if there's
# items, which ours doesn't have)
# kubectl get --raw /apis/admissionregistration.k8s.io/v1/validatingwebhookconfigurations | jq '.items[] | select(.metadata.name=="open-policy-agent-latest-helm-opa")'

# Now get the mutating webhook controller
kubectl get --raw /apis/admissionregistration.k8s.io/v1/mutatingwebhookconfigurations | jq
```

### Enabling with k3d

The controllers should be enabled by default, but if you want to either 
disable them or enable them, you can do so ***when you create** the cluster
with the k3d flag `--kube-apiserver-arg`, e.g.

```bash
# Example of enabling a controller
k3d cluster create dev \
--kube-apiserver-arg "enable-admission-plugins=MutatingAdmissionWebhook"
```

## Admission Webhooks

Unlike admission controllers, these webhooks are dynamic and can be enabled/removed
at any time. There are two types: `MutatingAdmissionWebhook` and `ValidatingAdmissionWebhook`. They are part of the control plane and are enabled
as non-beta from `v1.16+`.

To work they need the Admission Controllers `MutatingAdmissionWebhook` and 
`ValidatingAdmissionWebhook` enabled in the cluster.

**Admission Webhook Flow**

1. K8s API receives a request to modify some state (e.g. add an object)
1. Mutating webhooks are called to modify the incoming objects
1. Object is validated by the API server
1. Validating webhook is called and can reject or accept the request

Note that your mutating webhook might not be seeing the final object state,
as other mutations can occur after it. Only `ValidatingAdmissionWebhook`s
are guaranteed to see the `Object`'s final state.

### Important Considerations

Admission Webhooks are directly in the line of deployment to your cluster.
If they fail, get in a crash loop or have some inifite circular dependency,
you could put your cluster in a state where no changes can be made.

**Important Considerations**

* Is the webhook required to run and must be up all the time?
* What should be done if a request to the webhook fails?
* What if a cicular dependency is created with the webhook?
* What if deployment fails and ends in a crash loop?

# Creating a Webhook

There are a few pre-requisites for using a webhook with K8s:

1. A K8s cluster running
1. It needs a certificate
1. It requires the following configurations
   * `apiVersion: admissionregistration.k8s.io/v1`
   * `kind: MutatingWebhookConfiguration`
   * `webhook`
      * `name`
      * `clientConfig`
         * `caBundle` _(Certificate Authority from K8s server)_
         * `service` _(Our webhook service that will be handling the request)_
      * `rules` _(Just like "rules" in RBAC, tells which resource types it applies to)_
         * `operations` _(Like "verbs" in RBAC. No idea why they didn't stay consistent)_
         * `apiGroups` _(These are the apis that allow "CRUD-ing" all the `Kind`s of Objects in K8s)_
         * `apiVersions` _(The versions of the api groups)_
         * resources _(the types of `Kind` Objects, e.g. pods)_
   * [Many optional (or defaulted) ones](https://pkg.go.dev/k8s.io/api/admissionregistration/v1beta1#MutatingWebhook) as well
1. It must be a REST service hosted somewhere _(can be in or out of K8s)_

## Create a cluster

We'll keep it simple and create a k3d cluster named "dev":

```shell
# Create the cluster which will give us the admission
# controllers by default
k3d cluster create "dev"

# Check the controllers are enabled
#
# Mutating
kubectl get --raw /apis/admissionregistration.k8s.io/v1/mutatingwebhookconfigurations | jq

# Validating
kubectl get --raw /apis/admissionregistration.k8s.io/v1/validatingwebhookconfigurations | jq
```

## Create a Certificate

We'll need a certificate to be able to host our webhook with SSL _(which is required)_.

### Create CSR Configuration

**FILENAME: `csr.conf`**
```conf
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
DNS.1 = mutator
DNS.2 = mutator.mutate-webhook
DNS.3 = mutator.mutate-webhook.svc
DNS.4 = mutator.mutate-webhook.svc.cluster.local
```

**FILENAME: `mutate-csr.yaml`**
```yaml
#########################################################################
############################ mutate-csr.yaml ############################
#########################################################################
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: mutator.mutate-webhook
spec:
  groups:

    # This is a built in group that means "all authenticated users"
    - system:authenticated
    
  # Do NOT replace this, we'll do it below with the actual
  # CSR value (base 64 encoded)
  request: REPLACE_ME

  # Required as of v1
  signerName: kubernetes.io/kubelet-serving

  # This tells K8s how we're going to be using the certificate it'll
  # be signing for us. This is important so it can be authorized to
  # do the right things
  usages:
    - digital signature
    - key encipherment
    - server auth
```

## Create CSR

```shell
# We're going to be creating our webhook as "mutator" and put it in
# the namespace "mutate-webhook"
export WEBHOOK="mutator"
export NAMESPACE="mutate-webhook"
export CSR_NAME="${WEBHOOK}.${NAMESPACE}.svc"

# Create certificate key
openssl genrsa -out ${WEBHOOK}.key 2048

# Now create the CSR
# Notice the "system:nodes". This is required as of v1 because that's
# the only subject allowed to get a CSR for serving a certificate
# itself instead of just being a basic client of the API
openssl req -new -key ${WEBHOOK}.key -subj "/CN=system:node:${CSR_NAME}/O=system:nodes" -out ${WEBHOOK}.csr -config csr.conf

# Encode it to send via kubectl and replace in the
# YAML we created above
sed -i "s/REPLACE_ME/$(cat ${WEBHOOK}.csr | base64 | tr -d '\n')/g"  mutate-csr.yaml
```

## Submit and Approve the CSR

```shell
kubectl create -f mutate-csr.yaml

# Now approve it
kubectl certificate approve mutator.mutate-webhook

# NOTE: if anything went wrong, you can find out why using the command:
# kubectl get csr/mutator.mutate-webhook --output=jsonpath="{.status}" | jq
```

## Get The Certificate

```shell
# Download the cert from K8s and save it decoded for use later
kubectl get csr mutator.mutate-webhook -o jsonpath='{.status.certificate}' | openssl base64 -d -A -out ${WEBHOOK}.crt

# Now make our certificate trusted for our REST api
sudo cp ${WEBHOOK}.crt /usr/local/share/ca-certificates

# Trust the certificate
sudo update-ca-certificates

# The above should result in a ".pem" file being created in
# /etc/ssl/certs/mutator.pem

# We need a pfx file for .Net Core
openssl pkcs12 -export -out https.pfx -inkey mutator.key -in mutator.crt -password pass:something

# We're going to need to tell ASP.Net to use our certificate
export ASPNETCORE_Kestrel__Certificates__Default__Path=$(pwd)/https.pfx
export ASPNETCORE_Kestrel__Certificates__Default__Password=something
```

## Create the REST API

We'll be using .Net Core to create our REST service.

```shell
# Create the scaffolding
dotnet new webapi -o Mutator
```

Next we need to create the endpoint, and DTOs, for handling the communication from
the Admission Controller to our webhook. We'll start with a mutating webhook.


create name space
  kubectl create namespace mutate-webhook
Label namespace
  kubectl label namespace mutate-webhook mutator=enabled
apply
  kubectl apply -f mutator-deployment-svc-config.yaml -n mutate-webhook

-----
kubectl config view --raw --minify --flatten -o jsonpath='{.clusters[].cluster.certificate-authority-data}'